package supermarket.wallmartws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import supermarket.wallmart.service.IMesh;
import supermarket.wallmart.service.impl.MeshImpl;

/**
 * The Class WallmartWS.
 */
@WebService(serviceName="WallmartWS/insertMesh")
public class WallmartWS implements IWallmartWS {

	/** The mesh. */
	private IMesh mesh = new MeshImpl();

	/* (non-Javadoc)
	 * @see supermarket.wallmartws.IWallmartWS#insertMesh(java.lang.String, java.lang.String, java.lang.String, java.lang.Double)
	 */
	@Override
	@WebMethod
	public boolean insertMesh (
			@WebParam(name = "meshName") String meshName,
			@WebParam(name = "origin") String origin,
			@WebParam(name = "destination") String destination,
			@WebParam(name = "distance") Double distance) {
		return this.mesh.insert(meshName, origin, destination, distance);
	}

}
