package supermarket.wallmartws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * The Interface IWallmartWS.
 */
@WebService
public interface IWallmartWS {

	/**
	 * Insert mesh.
	 *
	 * @param meshName the mesh name
	 * @param origin the origin
	 * @param destination the destination
	 * @param distance the distance
	 * @return true, if successful
	 */
	@WebMethod
	boolean insertMesh (
			@WebParam(name = "meshName") String meshName,
			@WebParam(name = "origin") String origin,
			@WebParam(name = "destination") String destination,
			@WebParam(name = "distance") Double distance);

}
