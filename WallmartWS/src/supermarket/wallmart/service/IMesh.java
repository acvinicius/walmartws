package supermarket.wallmart.service;

import java.util.List;

import supermarket.wallmart.model.Mesh;

/**
 * The Interface IMesh.
 */
public interface IMesh {

	/**
	 * Gets the all mesh from db.
	 *
	 * @return the all mesh from db
	 */
	List<Mesh> getAllMeshFromDB();

	/**
	 * Insert.
	 *
	 * @param meshName the mesh name
	 * @param origin the origin
	 * @param destination the destination
	 * @param distance the distance
	 * @return true, if successful
	 */
	boolean insert(String meshName, String origin, String destination, Double distance);
}
