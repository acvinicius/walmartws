package supermarket.wallmart.service.impl;

import java.sql.SQLException;
import java.util.List;

import supermarket.wallmart.dao.MeshDAO;
import supermarket.wallmart.model.Mesh;
import supermarket.wallmart.service.IMesh;

/**
 * The Class MeshImpl.
 */
public class MeshImpl implements IMesh {

	/**
	 * Instantiates a new mesh impl.
	 */
	public MeshImpl() {

	}

	/** The mesh dao. */
	private MeshDAO meshDAO = new MeshDAO();

	/* (non-Javadoc)
	 * @see supermarket.wallmart.service.IMesh#insert(java.lang.String, java.lang.String, java.lang.String, java.lang.Double)
	 */
	public boolean insert(String meshName, String origin, String destination, Double distance) {
		Mesh mesh = new Mesh();
		mesh.setMeshName(meshName);
		mesh.setOrigin(origin);
		mesh.setDestination(destination);
		mesh.setDistance(distance);
		try {
			this.meshDAO.insertMesh(mesh);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see supermarket.wallmart.service.IMesh#getAllMeshFromDB()
	 */
	public List<Mesh> getAllMeshFromDB() {
		try {
			return this.meshDAO.getAllMeshFromDB();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
