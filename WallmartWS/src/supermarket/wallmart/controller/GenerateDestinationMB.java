package supermarket.wallmart.controller;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import supermarket.wallmart.model.Mesh;
import supermarket.wallmart.model.MeshDream;
import supermarket.wallmart.service.IMesh;
import supermarket.wallmart.service.impl.MeshImpl;
import dijkstra.DijkstraAlgorithm;
import dijkstra.Vertex;

/**
 * The Class GenerateDestinationMB.
 */
@RequestScoped
@ManagedBean
public class GenerateDestinationMB implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5204715547954658555L;

	/** The filter. */
	private MeshDream filter;

	/** The imesh. */
	private IMesh imesh;

	/** The result. */
	private String result = null;

	/**
	 * Instantiates a new generate destination mb.
	 */
	public GenerateDestinationMB() {
		this.filter = new MeshDream();
		this.imesh = new MeshImpl();
	}

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init(){}

	/**
	 * Gets the result.
	 *
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * Sets the result.
	 *
	 * @param result the new result
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * Gets the filter.
	 *
	 * @return the filter
	 */
	public MeshDream getFilter() {
		return filter;
	}

	/**
	 * Sets the filter.
	 *
	 * @param filter the new filter
	 */
	public void setFilter(MeshDream filter) {
		this.filter = filter;
	}

	/**
	 * Execute search.
	 */
	public void executeSearch() {
		setResult(createGraph(filter.getOrigin()));
	}

	/**
	 * Creates the graph.
	 *
	 * @param originFilter the origin filter
	 * @return the string
	 */
	private String createGraph(String originFilter) {
		List<Mesh> listMesh = this.imesh.getAllMeshFromDB();
		if (listMesh != null || !listMesh.isEmpty()) {
			List<dijkstra.Vertex> nodes;
			List<dijkstra.Edge> edges;
			nodes = new ArrayList<dijkstra.Vertex>();
			edges = new ArrayList<dijkstra.Edge>();
			dijkstra.Graph graph;
			for (Mesh mesh : listMesh) {
				for (dijkstra.Vertex v : nodes) {
					if (!mesh.getOrigin().equals(v.getId())) {
						nodes.add(new dijkstra.Vertex(mesh.getOrigin(), mesh.getOrigin()));
					}
				}
				dijkstra.Edge e = new dijkstra.Edge(mesh.getOrigin(), new dijkstra.Vertex(mesh.getOrigin(), mesh.getOrigin()), new dijkstra.Vertex(mesh.getDestination(), mesh.getDestination()), mesh.getDistance());
				edges.add(e);
			}
			graph = new dijkstra.Graph(nodes, edges);
			DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);		
			dijkstra.execute(new dijkstra.Vertex(originFilter, originFilter));
			LinkedList<dijkstra.Vertex> path = dijkstra.getPath(new dijkstra.Vertex(filter.getDestination(), filter.getDestination()));
			if (path != null) {
				StringBuilder result = new StringBuilder();
				result.append("rota ");
				for (Vertex vertex : path) {
					result.append(vertex.getName()).append(" ");
				}
				result.append("com custo de ");
				Double value = 0.00D;
				value = dijkstra.getShortestDistance(new Vertex(filter.getDestination(), filter.getDestination()));
				DecimalFormat f = new DecimalFormat("#.##");
				result.append(f.format(value/filter.getAutonomy()*filter.getFuelValue())).append(".");
				return result.toString();
			} else {
				return "N�o existe rota para este destino.";
			}
		}
		return "N�o existem malhas cadastradas.";

	}

}
