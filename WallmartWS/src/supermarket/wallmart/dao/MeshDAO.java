package supermarket.wallmart.dao;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.ejb.HibernatePersistence;

import supermarket.wallmart.model.Mesh;
import supermarket.wallmart.util.HibernateUtil;

/**
 * The Class MeshDAO.
 */
public class MeshDAO extends HibernatePersistence {

	/**
	 * Instantiates a new mesh dao.
	 */
	public MeshDAO() {

	}

	/**
	 * Insert mesh.
	 *
	 * @param mesh the mesh
	 * @return true, if successful
	 * @throws SQLException the SQL exception
	 */
	public boolean insertMesh(Mesh mesh) throws SQLException {
		try {
			SessionFactory sf = HibernateUtil.getSessionFactory();
			Session session = sf.openSession();
			session.beginTransaction();
			session.save(mesh);
			session.getTransaction().commit();
			session.close();
		} catch (HibernateException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Gets the all mesh from db.
	 *
	 * @return the all mesh from db
	 * @throws SQLException the SQL exception
	 */
	public List<Mesh> getAllMeshFromDB() throws SQLException {
		try {
			SessionFactory sf = HibernateUtil.getSessionFactory();
			Session session = sf.openSession();
			session.beginTransaction();
			Query q = session.createQuery("SELECT m FROM Mesh m");
			@SuppressWarnings("unchecked")
			List<Mesh> result = q.list();
			session.getTransaction().commit();
			session.close();
			return result;
		} catch (HibernateException e) {
			e.printStackTrace();
			return null;
		}
	}

}
