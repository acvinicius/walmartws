package supermarket.wallmart.test;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * The Class AllTests.
 */
public class AllTests {

	/**
	 * Suite.
	 *
	 * @return the test
	 */
	public static Test suite() {
		TestSuite suite = new TestSuite(AllTests.class.getName());
		//$JUnit-BEGIN$
		suite.addTestSuite(TestWallmartMeshImpl.class);
		suite.addTestSuite(TestGenerationDestinationMB.class);
		//$JUnit-END$
		return suite;
	}

}
