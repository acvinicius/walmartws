package supermarket.wallmart.test;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import dijkstra.DijkstraAlgorithm;
import dijkstra.Vertex;
import junit.framework.TestCase;
import supermarket.wallmart.model.Mesh;
import supermarket.wallmart.model.MeshDream;
import supermarket.wallmart.service.IMesh;
import supermarket.wallmart.service.impl.MeshImpl;

/**
 * The Class TestGenerationDestinationMB.
 */
public class TestGenerationDestinationMB extends TestCase {
	
	/** The filter. */
	private MeshDream filter;

	/** The imesh. */
	private IMesh imesh;

	/** The result. */
	private String result;
	
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		filter = new MeshDream();
		imesh = new MeshImpl();
		result = null;
		super.setUp();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	/**
	 * Instantiates a new test generation destination mb.
	 *
	 * @param name the name
	 */
	public TestGenerationDestinationMB(String name) {
		super(name);
	}
	
	/**
	 * Test execute search.
	 */
	public final void testExecuteSearch() {
		filter.setOrigin("TestA");
		filter.setDestination("TestB");
		filter.setAutonomy(5D);
		filter.setFuelValue(1.5D);
		result = testCreateGraph(filter.getOrigin());
		assertEquals("rota TestA TestB com custo de 1,5.", result);
	}

	/**
	 * Test create graph.
	 *
	 * @param originFilter the origin filter
	 * @return the string
	 */
	private String testCreateGraph(String originFilter) {
		List<Mesh> listMesh = this.imesh.getAllMeshFromDB();
		if (listMesh != null || !listMesh.isEmpty()) {
			List<dijkstra.Vertex> nodes;
			List<dijkstra.Edge> edges;
			nodes = new ArrayList<dijkstra.Vertex>();
			edges = new ArrayList<dijkstra.Edge>();
			dijkstra.Graph graph;
			for (Mesh mesh : listMesh) {
				for (dijkstra.Vertex v : nodes) {
					if (!mesh.getOrigin().equals(v.getId())) {
						nodes.add(new dijkstra.Vertex(mesh.getOrigin(), mesh.getOrigin()));
					}
				}
				dijkstra.Edge e = new dijkstra.Edge(mesh.getOrigin(), new dijkstra.Vertex(mesh.getOrigin(), mesh.getOrigin()), new dijkstra.Vertex(mesh.getDestination(), mesh.getDestination()), mesh.getDistance());
				edges.add(e);
			}
			graph = new dijkstra.Graph(nodes, edges);
			DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);		
			dijkstra.execute(new dijkstra.Vertex(originFilter, originFilter));
			LinkedList<dijkstra.Vertex> path = dijkstra.getPath(new dijkstra.Vertex(filter.getDestination(), filter.getDestination()));
			if (path != null) {
				StringBuilder result = new StringBuilder();
				result.append("rota ");
				for (Vertex vertex : path) {
					result.append(vertex.getName()).append(" ");
				}
				result.append("com custo de ");
				Double value = 0.00D;
				value = dijkstra.getShortestDistance(new Vertex(filter.getDestination(), filter.getDestination()));
				DecimalFormat f = new DecimalFormat("#.##");
				result.append(f.format(value/filter.getAutonomy()*filter.getFuelValue())).append(".");
				return result.toString();
			} else {
				return "N�o existe rota para este destino.";
			}
		}
		return "N�o existem malhas cadastradas.";
	}
	
}
