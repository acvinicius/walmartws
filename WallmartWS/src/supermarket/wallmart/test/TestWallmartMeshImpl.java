package supermarket.wallmart.test;

import java.util.ArrayList;
import java.util.List;

import supermarket.wallmart.model.Mesh;
import supermarket.wallmart.service.IMesh;
import supermarket.wallmart.service.impl.MeshImpl;
import junit.framework.TestCase;

/**
 * The Class TestWallmartMeshImpl.
 */
public class TestWallmartMeshImpl extends TestCase {
	
	/** The imesh. */
	private IMesh imesh;

	/**
	 * Instantiates a new test wallmart mesh impl.
	 *
	 * @param name the name
	 */
	public TestWallmartMeshImpl(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		this.imesh = new MeshImpl();
		super.setUp();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test insert.
	 */
	public final void testInsert() {
		Mesh mesh = new Mesh();
		mesh.setMeshName("Test");
		mesh.setOrigin("TestA");
		mesh.setDestination("TestB");
		mesh.setDistance(5D);
		imesh.insert(mesh.getMeshName(), mesh.getOrigin(), mesh.getDestination(), mesh.getDistance());
		List<Mesh> list = new ArrayList<Mesh>();
		list = imesh.getAllMeshFromDB();
		for (Mesh meshItem : list) {
			if (meshItem.getMeshName().equals(mesh.getMeshName())) {
				assertEquals(mesh.getOrigin(), meshItem.getOrigin());
				assertEquals(mesh.getDestination(), meshItem.getDestination());
				assertEquals(mesh.getDistance(), meshItem.getDistance());
			}
		}
	}

}
