package supermarket.wallmart.model;

/**
 * The Class MeshDream.
 */
public class MeshDream {

	/** The origin. */
	private String origin;

	/** The destination. */
	private String destination;

	/** The autonomy. */
	private Double autonomy;

	/** The fuel value. */
	private Double fuelValue;

	/**
	 * Gets the origin.
	 *
	 * @return the origin
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * Sets the origin.
	 *
	 * @param origin the new origin
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * Gets the destination.
	 *
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * Sets the destination.
	 *
	 * @param destination the new destination
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * Gets the autonomy.
	 *
	 * @return the autonomy
	 */
	public Double getAutonomy() {
		return autonomy;
	}

	/**
	 * Sets the autonomy.
	 *
	 * @param autonomy the new autonomy
	 */
	public void setAutonomy(Double autonomy) {
		this.autonomy = autonomy;
	}

	/**
	 * Gets the fuel value.
	 *
	 * @return the fuel value
	 */
	public Double getFuelValue() {
		return fuelValue;
	}

	/**
	 * Sets the fuel value.
	 *
	 * @param fuelValue the new fuel value
	 */
	public void setFuelValue(Double fuelValue) {
		this.fuelValue = fuelValue;
	}

}
