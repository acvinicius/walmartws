package supermarket.wallmart.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Mesh.
 */
@Entity
@Table(name = "TBMESH")
public class Mesh implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6807992169402314512L;

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false, name = "mesh_id")
	private Long id;

	/** The mesh name. */
	@Column(name = "meshname", nullable = false)
	private String meshName;

	/** The origin. */
	@Column(name = "origin", nullable = false)
	private String origin;

	/** The destination. */
	@Column(name = "destination", nullable = false)
	private String destination;

	/** The distance. */
	@Column(name = "distance", nullable = false)
	private Double distance;

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Gets the origin.
	 *
	 * @return the origin
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * Gets the mesh name.
	 *
	 * @return the mesh name
	 */
	public String getMeshName() {
		return meshName;
	}

	/**
	 * Sets the mesh name.
	 *
	 * @param meshName the new mesh name
	 */
	public void setMeshName(String meshName) {
		this.meshName = meshName;
	}

	/**
	 * Sets the origin.
	 *
	 * @param origin the new origin
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * Gets the destination.
	 *
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * Sets the destination.
	 *
	 * @param destination the new destination
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * Gets the distance.
	 *
	 * @return the distance
	 */
	public Double getDistance() {
		return distance;
	}

	/**
	 * Sets the distance.
	 *
	 * @param distance the new distance
	 */
	public void setDistance(Double distance) {
		this.distance = distance;
	}
}
