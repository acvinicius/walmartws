package supermarket.wallmart.util;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * The listener interface for receiving hibernate events.
 * The class that is interested in processing a hibernate
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addHibernateListener<code> method. When
 * the hibernate event occurs, that object's appropriate
 * method is invoked.
 *
 * @see HibernateEvent
 */
public class HibernateListener implements ServletContextListener {

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent event) { 
		HibernateUtil.getSessionFactory();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent event) { 
		HibernateUtil.getSessionFactory().close(); 
	}
}

