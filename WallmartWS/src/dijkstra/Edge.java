package dijkstra;

/**
 * The Class Edge.
 */
public class Edge  {

	/** The id. */
	private final String id; 

	/** The source. */
	private final Vertex source;

	/** The destination. */
	private final Vertex destination;

	/** The weight. */
	private final Double weight; 

	/**
	 * Instantiates a new edge.
	 *
	 * @param id the id
	 * @param source the source
	 * @param destination the destination
	 * @param weight the weight
	 */
	public Edge(String id, Vertex source, Vertex destination, Double weight) {
		this.id = id;
		this.source = source;
		this.destination = destination;
		this.weight = weight;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Gets the destination.
	 *
	 * @return the destination
	 */
	public Vertex getDestination() {
		return destination;
	}

	/**
	 * Gets the source.
	 *
	 * @return the source
	 */
	public Vertex getSource() {
		return source;
	}

	/**
	 * Gets the weight.
	 *
	 * @return the weight
	 */
	public Double getWeight() {
		return weight;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return source + " " + destination;
	}


} 