
GUIA:

Pr� ajustes necess�rios:
� preciso criar o schema no bando de dados e atualizar os dados com endere�o, usu�rio e senha no arquivo hibernate.cfg.xml
A cria��o da tabela � feita automaticamente na primeira utiliza��o, seja em busca ou tentativa de inser��o de dados.

DEFINI��ES:
Foi criado um Webservice para inser��o de malhas no BD com o nome da malha.
N�o foi definido na especifica��o que a busca seria via Webservice, com isto gerei a busca via Website no endere�o generateDestination.xhtml.
Nos filtros definidos n�o consta que seria poss�vel escolher uma malha espec�fica para filtro. Pode ser implementado futuramente um filtro por malhas.
Com isto as malhas inseridas n�o podem ter o nome de origem e destino repetidos em malhas diferentes, n�o � garantido o funcionamento correto do sistema.
As rotas s�o origem-destino, para o caminho inverso existir ele tamb�m tem que ser inserido no BD. Ex:(A->B � diferente de B->A, � poss�vel inserir 'A B 10' e 'B A 2').
  

UTILIZA��O:
Utiliza-se o WallmartWSClient para inserir os dados no sistema. A persistencia � feita automaticamente.
Pode se criar o pr�prio Web Service Client com o endere�o do Servidor: http://localhost:8080/WallmartWS/services/WallmartWS?wsdl

BUSCA:
http://localhost:8080/WallmartWS/generateDestination.xhtml
Inseresse os dados da busca da melhor rota e o resultado � exibido na mesma p�gina.

IMPLEMENTA��O:
Como o objetivo do teste, ao meu ver, n�o � desenvolver um c�digo de busca em profundidade em grafos e sim avaliar meu conhecimento na liguagem, 
foi utilizado uma implementa��o j� existente presente no package dijkstra, que � a implementa��o necess�ria para busca em grafo.
Esta implementa��o esta dispon�vel em http://www.vogella.com/tutorials/JavaAlgorithmsDijkstra/article.html

Utilizei o Hibernate como forma de persistencia e busca utilizando hql devido a experi�ncias anteriores.
Utilizei managed beans para cria��o da interface web tamb�m por experi�ncia e por achar que � um tecnologia melhor.
O Webservice e o WebserviceClient foram gerados via eclipse a partir do momento que as classes e interfaces estavam criadas.
 