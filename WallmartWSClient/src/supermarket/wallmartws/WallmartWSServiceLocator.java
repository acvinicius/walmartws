/**
 * WallmartWSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package supermarket.wallmartws;

public class WallmartWSServiceLocator extends org.apache.axis.client.Service implements supermarket.wallmartws.WallmartWSService {

    public WallmartWSServiceLocator() {
    }


    public WallmartWSServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WallmartWSServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WallmartWS
    private java.lang.String WallmartWS_address = "http://localhost:8080/WallmartWS/services/WallmartWS";

    public java.lang.String getWallmartWSAddress() {
        return WallmartWS_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WallmartWSWSDDServiceName = "WallmartWS";

    public java.lang.String getWallmartWSWSDDServiceName() {
        return WallmartWSWSDDServiceName;
    }

    public void setWallmartWSWSDDServiceName(java.lang.String name) {
        WallmartWSWSDDServiceName = name;
    }

    public supermarket.wallmartws.WallmartWS getWallmartWS() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WallmartWS_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWallmartWS(endpoint);
    }

    public supermarket.wallmartws.WallmartWS getWallmartWS(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            supermarket.wallmartws.WallmartWSSoapBindingStub _stub = new supermarket.wallmartws.WallmartWSSoapBindingStub(portAddress, this);
            _stub.setPortName(getWallmartWSWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWallmartWSEndpointAddress(java.lang.String address) {
        WallmartWS_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (supermarket.wallmartws.WallmartWS.class.isAssignableFrom(serviceEndpointInterface)) {
                supermarket.wallmartws.WallmartWSSoapBindingStub _stub = new supermarket.wallmartws.WallmartWSSoapBindingStub(new java.net.URL(WallmartWS_address), this);
                _stub.setPortName(getWallmartWSWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WallmartWS".equals(inputPortName)) {
            return getWallmartWS();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://wallmartws.supermarket", "WallmartWSService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://wallmartws.supermarket", "WallmartWS"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WallmartWS".equals(portName)) {
            setWallmartWSEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
