package supermarket.wallmartws.test;

import java.rmi.RemoteException;

import supermarket.wallmartws.WallmartWS;
import supermarket.wallmartws.WallmartWSProxy;


/**
 * The Class WallmartWSClient.
 */
public class WallmartWSClient {

	/** The ws. */
	private static WallmartWS ws = new WallmartWSProxy();

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main (String args[]) {
		try {
			System.out.println(ws.insertMesh("First", "E", "C", 31D));
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
