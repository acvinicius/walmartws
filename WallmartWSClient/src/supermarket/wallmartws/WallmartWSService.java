/**
 * WallmartWSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package supermarket.wallmartws;

public interface WallmartWSService extends javax.xml.rpc.Service {
    public java.lang.String getWallmartWSAddress();

    public supermarket.wallmartws.WallmartWS getWallmartWS() throws javax.xml.rpc.ServiceException;

    public supermarket.wallmartws.WallmartWS getWallmartWS(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
