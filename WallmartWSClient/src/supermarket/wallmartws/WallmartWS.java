/**
 * WallmartWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package supermarket.wallmartws;

public interface WallmartWS extends java.rmi.Remote {
    public boolean insertMesh(java.lang.String meshName, java.lang.String origin, java.lang.String destination, double distance) throws java.rmi.RemoteException;
}
