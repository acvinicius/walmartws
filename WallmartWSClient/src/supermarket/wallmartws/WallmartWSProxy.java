package supermarket.wallmartws;

public class WallmartWSProxy implements supermarket.wallmartws.WallmartWS {
  private String _endpoint = null;
  private supermarket.wallmartws.WallmartWS wallmartWS = null;
  
  public WallmartWSProxy() {
    _initWallmartWSProxy();
  }
  
  public WallmartWSProxy(String endpoint) {
    _endpoint = endpoint;
    _initWallmartWSProxy();
  }
  
  private void _initWallmartWSProxy() {
    try {
      wallmartWS = (new supermarket.wallmartws.WallmartWSServiceLocator()).getWallmartWS();
      if (wallmartWS != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wallmartWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wallmartWS)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wallmartWS != null)
      ((javax.xml.rpc.Stub)wallmartWS)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public supermarket.wallmartws.WallmartWS getWallmartWS() {
    if (wallmartWS == null)
      _initWallmartWSProxy();
    return wallmartWS;
  }
  
  public boolean insertMesh(java.lang.String meshName, java.lang.String origin, java.lang.String destination, double distance) throws java.rmi.RemoteException{
    if (wallmartWS == null)
      _initWallmartWSProxy();
    return wallmartWS.insertMesh(meshName, origin, destination, distance);
  }
  
  
}